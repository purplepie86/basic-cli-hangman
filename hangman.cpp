#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <string>

using namespace std;

bool loadwords(const char *, vector<string> &);
void hangmanASCIIstatus(int score);
void greeting();
void prompt(int &h);
void wordsize();
void wordstatus(int &b);

std::string word;
std::string word_guess;
std::string guesses;
std::string hidden_word;

int main() {

    int score = 0;

	srand(time(NULL));

	vector<string> wordlist;
	if (loadwords("./hangmanwords.txt",wordlist)) {

        word = wordlist.at(rand() % wordlist.size());
    }

	else {

		std::cout << "There was an error opening a file.\n";
    }

    wordsize();
    greeting();

    while (word_guess != word && hidden_word != word && score < 7) {

        std::cout << "\t" << hidden_word << "\n\n";

        hangmanASCIIstatus(score);
        prompt(score);
        wordstatus(score);
    }

    if (word_guess == word || hidden_word == word) {

        std::cout << "Congrats! You guessed the word correctly!\n";
        
        return 0;
    }

    if (score == 7 && word_guess != word) {

        hangmanASCIIstatus(score);

        std::cout << "Try again next time!\n";

        return 0;
    }
}

void wordsize() {

    for (int i = 0; i < word.size(); i++) {

        hidden_word += "_";
    }
}

void wordstatus(int &b) {

    bool correct = false;

    while (correct == false) {

        for (int j = 0; j < word.size(); j++) {

            for (int k = 0; k < guesses.size(); k++) {

                if (word[j] == guesses[k]) {

                    hidden_word[j] = guesses[k];

                    std::cout << "You guessed correct!\n";
                    guesses[k];
                
                    if (b != 0) {

                        b -= 1;
                    }

                    correct = true;
                }
            }
        }

        correct = true;
    }
}

void prompt(int &h) {

    std::string letter_guess;
    std::string letterorword;

    do {

        std::cout << "Would you like to guess a letter or word? (l/w): ";
        std::cin >> letterorword;
    }
    while (letterorword != "l" && letterorword != "w");

    if (letterorword == "l") {

        bool duplicate = false;

        do {

            duplicate = false;

            std::cout << "Input the letter you would like to guess: ";
            std::cin >> letter_guess;

            for (int l = 0; l < guesses.size(); l++) {

                if (guesses[l] == letter_guess[0]) {

                    duplicate = true;

                    std::cout << "You guessed a letter you've already guessed previously! Please guess a different letter.\n";
                }
            }
        }
        while (duplicate == true);

        guesses += letter_guess;

        h += 1;
    }

    if (letterorword == "w") {
        
        std::cout << "Input the word you would like to guess: ";
        std::cin >> word_guess;

        std::cout << "If the game does not end then you guessed the word incorrectly.\n";

        h += 1;
    }
}

void greeting() {

    std::cout << "\nWelcome to Hangman! You'll have 7 tries in total to guess the chosen word. If you guess correctly, then it does not count towards your total of 7 tries. On each try, you can either attempt to guess a letter or the entire word. Both actions count as a single guess.\n";
}

bool loadwords(const char * filename, vector<string> & words)
{
	fstream file(filename,std::ios::in);
	char buffer[30];

	if (file.is_open()) {

		words.clear();
		while (!file.eof())
		{

			file.getline(buffer, 30);
			words.push_back(buffer);
		}
		file.close();
		return true;
	}

	else {

		return false;
    }
}

void hangmanASCIIstatus(int score) {

    if (score == 0) {

        std::cout << "\t  _________\n\t|/\n\t|\n\t|\n\t|\n\t|\n\t|___\n\nFailed guesses: 0\n\n";
        }

    if (score == 1) {

        std::cout << "\t  _________\n\t|/   |\n\t|\n\t|\n\t|\n\t|\n\t|___\n\nFailed guesses: 1\n\n";
    }

    if (score == 2) {

        std::cout << "\t  _________\n\t|/   |\n\t|   (_)\n\t|\n\t|\n\t|\n\t|___\n\nFailed guesses: 2\n\n";
    }

    if (score == 3) {

        std::cout << "\t  _________\n\t|/   |\n\t|   (_)\n\t|    |\n\t|    |\n\t|\n\t|___\n\nFailed guesses: 3\n\n";
    }

    if (score == 4) {

        std::cout << "\t  _________\n\t|/   |\n\t|   (_)\n\t|   /|\n\t|    |\n\t|\n\t|___\n\nFailed guesses: 4\n\n";
    }

    if (score == 5) {

        std::cout << "\t  _________\n\t|/   |\n\t|   (_)\n\t|   /|\\\n\t|    |\n\t|\n\t|___\n\nFailed guesses: 5\n\n";
    }

    if (score == 6) {

        std::cout << "\t  _________\n\t|/   |\n\t|   (_)\n\t|   /|\\\n\t|    |\n\t|   /\n\t|___\n\nFailed guesses: 6\n\n";
    }

    if (score == 7) {

        std::cout << "\t  _________\n\t|/   |\n\t|   (_)\n\t|   /|\\\n\t|    |\n\t|   / \\\n\t|___\n\nFailed gluesses: 7\n\n";
    }
}